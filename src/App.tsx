import { Navigate, Route, Routes } from "react-router-dom";
import DocumentPage from "./pages/DocumentPage";
import HomePage from "./pages";

export default function App() {
  return (
    <>
      <Routes>
        <Route path="/docs" element={<DocumentPage />} />
        <Route path="/" element={<HomePage />} />
        <Route path="*" element={<Navigate to="/" />} />
      </Routes>
    </>
  );
}
