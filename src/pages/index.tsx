import { useState } from "react";
import reactLogo from "@/assets/react.svg";
import viteLogo from "/vite.svg";
import "../App.css";
import gitSrc from "@/assets/images/git-logo.png";
import gitlabSrc from "@/assets/images/R.png";
import { useNavigate } from "react-router-dom";

export default function HomePage() {
  const [count, setCount] = useState(0);
  const navigate = useNavigate();
  return (
    <>
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          backgroundColor: "whitesmoke",
          borderRadius: 10,
        }}
      >
        <a href="https://vitejs.dev" target="_blank">
          <img src={viteLogo} className="logo" alt="Vite logo" />
        </a>
        <a href="https://react.dev" target="_blank">
          <img src={reactLogo} className="logo react" alt="React logo" />
        </a>
        <a href="https://git-scm.com/" target="_blank">
          <img src={gitSrc} className="logo" alt="git logo" />
        </a>
        <a href="https://docs.gitlab.com/" target="_blank">
          <img src={gitlabSrc} className="logo" alt="gitlab logo" />
        </a>
      </div>
      <h1>GitLab CI/CD Workshop (Vite + React)</h1>
      <div className="card">
        <button onClick={() => setCount((count) => count + 1)}>
          count is {count}
        </button>
        <p>
          <button
            style={{
              color: "black",
              backgroundColor: "whitesmoke",
            }}
            onClick={() => {
              navigate("/docs");
            }}
          >
            Navigate to document 📃
          </button>
        </p>
      </div>
      <div>
        <a
          href="https://gitlab.com/redcats002/gitlab-basic-workshop"
          target="_blank"
        >
          <code> 🔗 Visit repository</code>
        </a>
      </div>
    </>
  );
}
