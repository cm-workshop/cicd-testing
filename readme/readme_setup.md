# Setup project

[Basic concept & Tutorial refer from](https://dev.to/christianmontero/gitlab-ci-cd-example-with-a-dockerized-reactjs-app-1cda)

## Setup react with vite

```bash
# yarn
yarn create vite my-app --template react-ts
```

[Vite Guild](https://vitejs.dev/guide/)

## OR clone

First clone the repository you want to work with. This step could be skipped if you want it all to happen in the folder you are already in.

```bash
git clone file:///path/to/repo/
```

Cloning will bring over the remotes specified in that directory. So you'll need to remove the remotes you don't want.

```bash
git remote rm <remote>
```

And add the ones you do, after you have created your remote repository.

```bash
git remote add origin <url>
```

Run git pull to make local repository up-to-date to remote repository:

```bash
git pull
```

But it will error:
casuse you haven't tracking any change to remote branch correctly

```bash
There is no tracking information for the current branch.
Please specify which branch you want to merge with.
See git-pull(1) for details.

    git pull <remote> <branch>

If you wish to set tracking information for this branch you can do so with:

    git branch --set-upstream-to=origin/<branch> main
```

In this case you just typing:

```bash
git branch --set-upstream-to=origin/main main
```

And another error will be returned:

```bash
$ git pull
fatal: refusing to merge unrelated histories
```

When you set up the tracking information, Git expects the histories to be related, meaning they have a common ancestor. However, in some scenarios, the histories might have diverged too much for Git to automatically merge them.

To resolve this issue, you can try pulling with the --allow-unrelated-histories option, which will allow the merge to proceed even if the histories are unrelated. Here's how you can do it:

```bash
git pull --allow-unrelated-histories
```

After running this command, Git should proceed with the merge, and you might have to resolve any potential conflicts that arise during the process.

If you're confident that your local branch should override the remote branch completely (and you don't need any of the remote history), you can force the push instead of pulling and merging:

```bash
git push --force origin main
```

You will also want to `--set-upstream-to`, or `-u` to tell git this is the remote repository this branch will update to, presuming you are on the main (or default) branch.

Then you'll need to decide which branches to keep and add to the remote. If you want to push all of them, just do `git push --mirror`. This will push all your tags and your remotes. But since we edited your remotes in an earlier step, it shouldn't be a problem.

If you only want to keep a few, you can `git push -u origin <branch>` each one you want.
